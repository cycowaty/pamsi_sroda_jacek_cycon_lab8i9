
#include "stdafx.h"

int znajdz(int i, int *U)
{
	int j;
	j = i;
	while (U[j] != j)
		j = U[j];
	return (j);
}

void quicksort(krawedzie* const tablica, int const a, int const b)
{
    int i = a, j = b;
    int v = tablica[div(a + b, 2).quot].waga, temp2;
    int temp0, temp1;
    do {
        while (tablica[i].waga < v) ++i;
        while (v < tablica[j].waga) --j;
        if (i <= j) {
            temp2 = tablica[i].waga;
            tablica[i].waga = tablica[j].waga;
            tablica[j].waga = temp2;
            temp1 = tablica[i].n;
            tablica[i].n = tablica[j].n;
            tablica[j].n = temp1;
            temp0 = tablica[i].p;
            tablica[i].p = tablica[j].p;
            tablica[j].p = temp0;
            i++;
            j--;
        }
    }
    while (i <= j);
    if (a < j) quicksort(tablica, a, j);
    if (i < b) quicksort(tablica, i, b);
}

void dodaj_krawedzie(int **M, krawedzie *k, int ile_w)
{
    int h = 0;
        for (int i=0; i < ile_w; i++)
		{
            for (int j=i; j < ile_w; j++)
			{
                if (M[i][j] != 0)
				{
                    k[h].p = i;
					k[h].n = j;
                    k[h].waga = M[i][j];
                    h++;
                }
            }
        }
}

int policz_krawedzie(int **M, int rozm)
{
	int k=0;
	for (int i=0; i < rozm; i++)
	{
		for(int j=i; j < rozm; j++)
		{
			if (M[i][j] != 0)
				k++;
		}
	}
	return k;
}

int policz_krawedzie(lista **L, int ile_w)
{
	int ilekr=0;
    wierzcholek *tmp;
    for (int i=0; i<ile_w; i++)
	{
		tmp = L[i]->w;
		while (tmp != NULL)
		{
			ilekr++;
            tmp = tmp->n;
        }
	}

	return ilekr;
}

void dodaj_krawedzie(lista **L, krawedzie *k, int ile_w)
{
	int j = 0;
    wierzcholek *tmp;
    for (int i = 0; i < ile_w; ++i)
	{
		tmp = L[i]->w;
        while (tmp != NULL)
		{
			k[j].p = i;
			k[j].n = tmp->indeks;
			k[j].waga = tmp->waga;
            tmp = tmp->n;
            j++;
        }
	}
}

krawedzie *kruskal(krawedzie *k, int ile_kraw, int ile_w)
{
	krawedzie* result = new krawedzie[ile_w];

	int *U = new int[ile_w+1];

	quicksort(k, 0, ile_kraw-1);			// sortujemy tablice krawedzi

	for (int i = 0; i < ile_w; i++)
		U[i] = i;							// tworzymy las wiercholkow

	int a, b, i, j;

	int numer = 0;
	int nastepna = 0;

	while (numer < ile_w - 1)
	{
		a = k[nastepna].p;					// krawedz poprzednia
		b = k[nastepna].n;					// krawedz nastepna

		i = znajdz(a, U);					// znajdujemy wierzcholki w lesie
		j = znajdz(b, U);

		if (i != j)
		{
			if (i < j)
				U[j] = i;					// dolaczamy nast krawedz do lasu
			else
				U[i] = j;

			result[numer].p = k[nastepna].p;			// dolaczamy nast krawedz do wyniku
			result[numer].n = k[nastepna].n;
			result[numer].waga = k[nastepna].waga;
			numer++;
		}
		nastepna++;
	}
	delete U;
	return result;
}

krawedzie* startKruskal (int **M, int ile_w)
{
	krawedzie *result = new krawedzie[ile_w];

    int ile_kraw = policz_krawedzie(M, ile_w);			// liczymy krawedzie
    krawedzie *edges = new krawedzie[ile_kraw];

	dodaj_krawedzie(M, edges, ile_w);					// tworzymy zbior krawedzi grafu
    result = kruskal(edges, ile_kraw, ile_w);

	delete edges;
    return result;
}

krawedzie* startKruskal(lista **L, int ile_w)
{
	int kr = policz_krawedzie(L, ile_w);
	krawedzie*edges = new krawedzie[kr];

	krawedzie* result = new krawedzie[ile_w];

	dodaj_krawedzie(L, edges, ile_w);
	result = kruskal(edges, kr, ile_w);

	delete edges;
	return result;
}
