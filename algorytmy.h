
//#include "stdafx.h"

// Prim
krawedzie* prim(int **M, int v);
krawedzie* prim(lista **L, int v);

// Kruskal
krawedzie* kruskal(krawedzie *k, int ile_kraw, int ile_w);

int policz_krawedzie(int **M, int rozm);
void dodaj_krawedzie(int **M, krawedzie *k, int ile_w);
krawedzie* startKruskal(int **M, int ile_w);

int policz_krawedzie(lista **L, int ile_w);
void dodaj_krawedzie(lista **L, krawedzie *k, int ile_w);
krawedzie* startKruskal(lista **L, int ile_w);

// Dijkstra
int* dijkstra(lista **L, int ile_w, int *poprzednie);
int* dijkstra(int **M, int ile_w, int *poprzednie);
int min_sciezka(int *tab, int *tab2, int ile_w);

