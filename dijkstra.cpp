
#include "stdafx.h"

//Algorytm Dijkstry dla grafu zapisanego w postaci listy
int* dijkstra(lista **L, int ile_w, int *poprzednie)
{
        int m;
        wierzcholek *t;
		int* result = new int[ile_w];

        int *sciezki= new int[ile_w];                   // tablica pomocnicza pamietajaca znalezione juz minimalne sciezki

        for(int i = 0; i < ile_w; i++)
        {
                sciezki[i] = 1;                         // wypelnienie '1' tablicy pomocniczej, czyli nie znamy jeszcze �adnej sciezki
                result[i] = MAX * ile_w;		        // wypelnienie tablicy wynikowej wartosciami sciezek maksymalnych

        }

        t = L[0] -> w;

        while(t != NULL)								// przegladanie listy
        {
                result[t -> indeks] = t -> waga;		// wypelnienie tablicy wynik�w, wartosciami odpowiadajacych wag
                t = t -> n;
        }
        sciezki[0] = 0;

        for(int j = 0; j < ile_w - 1; j++)					// wyszukiwanie minimalnych sciezek
        {
                m = min_sciezka(result, sciezki ,ile_w);	// wyszukujemy sciezke minimalna z juz znalezionych
                sciezki[m] = 0;								// zapami�tanie znalezionej sciezki
                t = L[m] -> w;
                while(t != NULL)							// relaksacja kraw�dzi
				{
					if(sciezki[t -> indeks] == 1)
															// sprawdzenie czy przy przejsciu przez dana krawedz nie otrzymamy kr�tszej �cie�ki
						if((result[m] + t -> waga) < result[t -> indeks])
						{
															// jesli tak, to aktualizujemy
							result[t -> indeks] = result[m] + t -> waga;
							poprzednie[t -> indeks]= m;
						}

                        t = t -> n;
                }
        }
        result[0] = 0;

		return result;
}

//Algorytm Dijkstry dla grafu zapisanego w postaci macierzowej
int* dijkstra(int **M, int ile_w, int *poprzednie)
{
        int min, *sciezki;
		int* result = new int[ile_w];

		sciezki = new int [ile_w];                      // tablica pomocnicza pamietajaca znalezione juz minimalne sciezki

        for(int i = 0; i < ile_w; i++)
        {
                sciezki[i] = 1;                         // wypelnienie '1', czyli nie znamy jeszcze �adnej sciezki

                if(M[0][i] != 0)						// wypelnienie tablicy wynikowej warto�ciami sciezek maksymalnych
					result[i] = M[0][i];				// dla kom�rek niezerowych - zapisujemy warto�� tej kom�rki
                else
					result[i] = MAX * ile_w;			// jezli brak krawedzi zapisujem wage maksymalna

        }

        sciezki[0] = 0;

        for(int j = 0; j < ile_w - 1; j++)							// wyszukiwanie minimalnych sciezek
        {
                min = min_sciezka(result, sciezki, ile_w);			// wyszukujemy sciezke minimalna z juz znalezionych
                sciezki[min] = 0;									// zapami�tanie znalezionej sciezki

                for(int w = 0; w < ile_w; w++)						// relaksacja krawedzi
					if((sciezki[w] == 1) && (M[min][w] != 0))
						if((result[min] + M[min][w]) < result[w])	// sprawdzenie czy przy przejsciu przez dana krawedz nie otrzymamy kr�tszej sciezki
						{
							poprzednie[w]=min;
							result[w] = result[min] + M[min][w];	// jesli tak, to aktualizujemy
						}
		}
        result[0] = 0;

		return result;
}


// Wyszukiwanie najkr�tszej �cie�ki ze wszystkich znalezionych dla danego wierzcho�ka
int min_sciezka(int *tab, int *tab2, int ile_w)
{
		int min , m_tab;

		min = 0;
		m_tab = tab[0];

        for (int i = 0 ; i < ile_w ; i++)                       // petla przegladajaca tablice
			if(tab2[i] == 1 && tab[i] < m_tab)					// je�li nie znamy jeszcze najkr�tszej sciezki dla danego wierzcholka
            {                                                   // i znaleziona droga jest kr�tsze od dotychczasowego minimum
				m_tab = tab[i];									// zapis nowych warto�ci minimalnych
                min = i;
            }

		return min;
}
