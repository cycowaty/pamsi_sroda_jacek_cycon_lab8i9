
#include "stdafx.h"

using namespace std;



krawedzie* prim(int **M, int v)
{
	int temp,temp2, min;
	int *wykorzystane = new int[v];				// tablica wykorzystanych wierzcholkow
	krawedzie* wyniki = new krawedzie[v];
	int suma = 0;
	for(int i = 0; i < v; i++)
		wykorzystane[i] = -1;

	wykorzystane[0] = 0;						// pierwszy elem. - pierwszy wierzcholek
	wyniki[0].p=0;

	for(int i = 1; i < v; i++)
	{
		temp = -1;
		min = MAX+1;
		for (int k = 0; k < v; k++)
		{
			if(wykorzystane[k] != -1)								// jezeli wykorzystany wierzcholek
			{
				for(int s = 0; s < v; s++)
				{
					if(wykorzystane[s] == -1)						// jezeli nie wykorzystany sasiad
					{
						if(M[k][s] != 0 && M[k][s] < min)			// jezeli waga mniejsza od min
						{
							min = M[k][s];							// ustawiamy minimum
							temp = s;
							temp2 = k;
						}
					}
				}
			}
		}
		wykorzystane[temp] = i;										// budujemy drzewo
		wyniki[i-1].n = temp;
		wyniki[i-1].p = temp2;
		wyniki[i-1].waga = min;
	}
	delete wykorzystane;
	return wyniki;
}

krawedzie* prim(lista **L, int v)
{
	int min;
	int *wykorzystane = new int[v];
	krawedzie* wyniki = new krawedzie[v];

	for(int z = 0; z < v; z++)
		wykorzystane[z] = -1;

	wykorzystane[0] = 0;

	for(int i = 1; i < v; i++)
	{
		wierzcholek *tmp;
		int temp = -1;
		int temp2 = -1;
		min=1000+1;
		for (int s = 0; s < v; s++)
		{
			if(wykorzystane[s] != -1)
			{
				tmp = L[s]->w;
				while(tmp != NULL)
				{
					if(wykorzystane[tmp->indeks] == -1)
					{
						if(tmp->waga < min)
						{
							min=tmp->waga;
							temp=tmp->indeks;
							temp2=s;
						}
					}
					tmp=tmp->n;
				}
			}
		}
		wykorzystane[temp] = i;
		wyniki[i-1].n = temp;
		wyniki[i-1].p = temp2;
		wyniki[i-1].waga = min;


	}
	delete wykorzystane;
	return wyniki;
}
