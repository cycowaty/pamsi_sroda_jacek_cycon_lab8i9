#pragma once
#include <iostream>

class wierzcholek
{
public:
	wierzcholek *n;			// wskaznik na nastepnik
	int indeks;
	int waga;
};
class krawedzie
{
public:
	int p;
	int n;
	int waga;

	krawedzie(): p(-1), n(-1), waga(-1) {}
};

class lista
{
public:
	wierzcholek *w;

	void dodaj(int indeks, int waga);
	void wypisz();
	void wypisz_do_pliku(std::ofstream& p);
};

class graf_lista
{
public:
	int v;							// liczba wierzcholków
	lista **list;

	graf_lista():v(0){}
	graf_lista(int s): v(s){
		list = new lista*[s];
	}

	void wypisz();
	void wypisz_do_pliku(std::ofstream& p);
	void konwertuj(int **M);		// konwersja macierz -> lista
};

class graf_macierz
{
public:
	int v;				// liczba wierzcholków
	int **macierz_wag;			// reprezentacja macierzy jako tab dwuwymiarowej liczb calkowitych

	graf_macierz();
	graf_macierz(int ile);	// konstruktor tworzacy macierz ile x ile
	graf_macierz(int **tab, int rozmiar); // konstruktor tworzacy macierz na podstawie dolaczonej tablicy

	void wylosuj(int wsp);		// metoda generowania losowych macierzy
	void wyczysc();				// wyzerowanie macierzy
	void wypisz();
	void wypisz_do_pliku(std::ofstream& p);
};


